import sys
import random
from copy import deepcopy


class App(object):
    """
    Game only updates after the user has entered a VALID action.
    New shapes randomly positioned along the x-axis.
    """
    cols = 20
    rows = 20
    shapes = [
        [[1, 1, 1, 1]],  # shape 1

        [[1, 0],
         [1, 0],
         [1, 1]],  # shape 2

        [[0, 1],
         [0, 1],
         [1, 1]],  # shape 3

        [[0, 1],
         [1, 1],
         [1, 0]],  # shape 4

        [[1, 1],
         [1, 1]]  # shape 5
    ]

    def __init__(self):
        self.board = [[0 for x in xrange(self.cols)] for y in xrange(self.rows)]
        self.add_new_shape()  # The game starts with a random piece appearing at the top of the board.
        print("Control keys:\n"
              "  a - move piece left\n"
              "  d - move piece right\n"
              "  w - rotate piece counter clockwise\n"
              "  s - rotate piece clockwise")
        self.ask_move_to_user()

    def add_new_shape(self):
        """
        Adds new random shape to game with random x axis if board have free space for it.
        If this new shape had no valid move, then program exits.
        """
        self.shape = random.choice(self.shapes)
        self.shape_y = 0
        self.shape_x = random.randint(0, self.cols - len(self.shape[0]))
        if self.check_collision(self.shape, (self.shape_x, self.shape_y)):
            print("Game Over")
            sys.exit()

    def check_collision(self, shape, offset):
        """
        Checks given shape with given location (offset) acording to the board.
        If shape is not outside the bounds of the board or does not overlap with any existing shapes, returns False.
        """
        offset_x, offset_y = offset
        for y_counter, row in enumerate(shape):
            for x_counter, col_value in enumerate(row):
                try:
                    if col_value and self.board[y_counter + offset_y][x_counter + offset_x]:
                        return True
                except IndexError:
                    return True
        return False

    def rotate_clockwise(self, shape, counter=False):
        """
        Given shape returns after rotates clockwise or counter clockwise.
        """
        for z in xrange(1 if counter is True else 3):
            shape = [[shape[y][x] for y in xrange(len(shape))] for x in xrange(len(shape[0]) - 1, -1, -1)]
        return shape

    def fix_shape(self, save_board=True):
        """
        Joins the board with current shape with current offset.
        """
        board_state = deepcopy(self.board)
        for y_counter, row in enumerate(self.shape):
            for x_counter, col_value in enumerate(row):
                board_state[y_counter + self.shape_y][x_counter + self.shape_x] = '*' if col_value else ' '
        if save_board:
            self.board = board_state
        return board_state

    def print_board(self):
        """
        Prints tetris board's current state only using with "*" char.
        """
        board_state = self.fix_shape(save_board=False)
        for row in board_state:
            print('*{}*'.format(''.join(map(str, row)).replace('0', ' ')))
        print('*' * (self.cols + 2))

    def ask_move_to_user(self, warning=None):
        """
        Asks user to next move, checks validity and updates game if user has entered a valid action.
        """
        self.print_board()
        action = raw_input('{}Enter your move: '.format('%s\n' % warning if warning else ''))
        if action not in ['a', 'd', 'w', 's']:
            self.ask_move_to_user('(Control keys only)')
        if action in ['a', 'd']:
            x_diff = -1 if action == 'a' else 1
            new_x = self.shape_x + x_diff
            if self.check_collision(self.shape, (new_x, self.shape_y)):
                self.ask_move_to_user(warning='(Shape can not move with {})'.format(action))
            else:
                # Game only updates after the user has entered a valid action
                self.shape_x = new_x
        else:
            rotated_shape = self.rotate_clockwise(self.shape, counter=True if action == 'w' else False)
            if self.check_collision(rotated_shape, (self.shape_x, self.shape_y)):
                self.ask_move_to_user(warning='(Shape can not rotate with {})'.format(action))
            else:
                # Game only updates after the user has entered a valid action
                self.shape = rotated_shape

        if self.check_collision(self.shape, (self.shape_x, self.shape_y + 1)):
            # The pieces new position is not allow a new valid move
            self.fix_shape()
            self.add_new_shape()
        else:
            self.shape_y += 1
            self.ask_move_to_user()


if __name__ == '__main__':
    App = App()
